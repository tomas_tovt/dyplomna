<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('/book-catecory/{id}', 'BookController@category')->name('book-category');
Route::get('/book/{id}', 'BookController@book')->name('book');
Route::get('/popular', 'BookController@popular')->name('popular');

Auth::routes();
