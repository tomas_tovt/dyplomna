<?php

namespace App\Http\Sections\Common;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;

/**
 * Class Slider
 *
 * @property \App\Slider $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Slider extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::text('id', '# ID')->setWidth('50px'),
                AdminColumn::text('header', __('admin.slider_header')),
                AdminColumn::text('created_at', __('admin.created_at')),
                AdminColumn::text('updated_at', __('admin.updated_at')),
            ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
       return AdminForm::panel()->addBody([
           AdminFormElement::text( 'header', __('admin.slider_header') )->required(),
           AdminFormElement::text( 'second_text', __('admin.slider_second_text') )->required(),
           AdminFormElement::text( 'description', __('admin.slider_description') )->required(),
           AdminFormElement::text( 'link', __('admin.slider_link') )->required(),
           AdminFormElement::image( 'bg_image', __('admin.slider_bg_image') )->required(),
           AdminFormElement::image( 'image', __('admin.slider_image') )->required(),
       ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
