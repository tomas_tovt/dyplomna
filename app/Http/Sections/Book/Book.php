<?php

namespace App\Http\Sections\Book;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;

/**
 * Class Slider
 *
 * @property \App\Slider $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Book extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::text('id', '# ID')->setWidth('50px'),
                AdminColumn::text('name', __('admin.b_name')),
                AdminColumn::text('category.name', __('admin.b.category_name')),
                #AdminColumn::text('author.name', __('admin.b.category_name')),
                AdminColumn::text('created_at', __('admin.created_at')),
                AdminColumn::text('updated_at', __('admin.updated_at')),
            ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
       return AdminForm::panel()->addBody([
           AdminFormElement::text( 'name', __('admin.b_name') )->required(),
           AdminFormElement::image( 'image', __('admin.b_image') )->required(),
           AdminFormElement::select( 'category_id', __('admin.b_category'), 'App\Models\Book\BookCategory' )->setDisplay('name')->required(),
           AdminFormElement::textarea( 'description', __('admin.b_description') )->required(),
           AdminFormElement::text( 'author', __('admin.b_author') )->required(),
           AdminFormElement::text( 'publisher', __('admin.b_publisher') )->required(),
           AdminFormElement::text( 'year', __('admin.b_year') )->required(),
       ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
