<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Common\Slider;

class IndexController extends Controller
{
    public function index()
    {
        $cSliders = Slider::all();
        return view('index')->with([
            'sliders' => $cSliders
        ]);
    }

    public function favorites()
    {
        return view('favorites')->with([
            'favorites' => 0
        ]);
    }

}
