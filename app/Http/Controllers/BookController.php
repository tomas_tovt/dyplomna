<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book\Book;
use App\Models\Book\BookCategory;

class BookController extends Controller
{
    public function category($id){

        if(!$category = BookCategory::find($id)) return abort(404);
        $books = Book::where('category_id', $id)->paginate(8);

        return view('category')->with([
            'books' => $books,
            'category' => $category
        ]);
    }
}
