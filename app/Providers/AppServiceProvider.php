<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use App\Models\Book\BookCategory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //its just a dummy data object.
        $menu_categories = BookCategory::all();

        // Sharing is caring
        \View::share('menu_categories', $menu_categories);
    }
}
