<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
       \App\Models\Common\Slider::class => 'App\Http\Sections\Common\Slider',
       \App\Models\Book\BookCategory::class => 'App\Http\Sections\Book\BookCategory',
       \App\Models\Book\Book::class => 'App\Http\Sections\Book\Book',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	parent::boot($admin);
    }
}
