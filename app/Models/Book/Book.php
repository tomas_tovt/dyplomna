<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book\BookCategory;

class Book extends Model
{
    public function category()
    {
        return $this->hasOne('BookCategory');
    }
}
