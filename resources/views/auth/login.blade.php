@extends('layouts.ebook')

@section('content')

    <!--// SubHeader \\-->
    <div class="ereaders-subheader">
        <div class="ereaders-subheader-text">
            <span class="ereaders-subheader-transparent"></span>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{ __('ebook.login_title') }}</h1>
                        <p>{{ __('ebook.login_title_secondary') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="ereaders-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li><a href="/">{{ __('ebook.homepage') }}</a></li>
                            <li class="active">{{ __('ebook.login_title') }}</li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--// SubHeader \\-->
    <div class="ereaders-main-content">
        <!--// Main Section \\-->
        <div class="ereaders-main-section">
            <div class="container">
                <div class="row justify-content-center ">

                    <div class="col-md-12">
                        <div class="ereaders-fancy-title text-center">
                            <h2>{{ __('ebook.login_title') }}</h2>
                            <h6><a href="{{ route('register') }}">{{ __('ebook.register_text') }}</a></h6>
                         </div>
                    </div>
                      <div class="col-md-4 col-lg-offset-4">

                        <form action="{{ route('login') }}" method="post" >
                            @csrf
                            <div class="form-group">
                                <label for="email">{{ __('ebook.e-mail') }}</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror

                            </div>
                            <div class="form-group">
                                <label for="pwd">{{ __('ebook.password') }}</label>
                                <input type="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="checkbox">
                                <label> <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('ebook.remember_me') }}</label>

                            </div>
                            <button type="submit" class="btn btn-primary">
                                {{ __('ebook.login') }}
                            </button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>

@endsection
