<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <!-- Css Files -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/flaticon.css" rel="stylesheet">
    <link href="/css/slick-slider.css" rel="stylesheet">
    <link href="/css/fancybox.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/color.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--// Main Wrapper \\-->
<div class="ereaders-main-wrapper">

    <!--// Header \\-->
    <header id="ereaders-header" class="ereaders-header-one">
        <div class="ereaders-main-header">
            <div class="container">
                <div class="row">
                    <aside class="col-md-3"> <a href="/" class="logo"><img src="/images/logo.png" alt=""></a> </aside>
                    <aside class="col-md-9">
                        <!--// Navigation \\-->
                        <a href="#menu" class="menu-link active"><span></span></a>
                        <nav id="menu" class="menu navbar navbar-default">
                            <ul class="level-1 navbar-nav">
                                <li class="active"><a href="/">{{ __('menu.home') }}</a></li>
                                <li><a href="#">{{ __('menu.books') }}</a><span class="has-subnav"><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub-menu level-2">
                                        @foreach($menu_categories as $category)
                                            <li><a href="{{ route('book-category', ['id'=> $category->id ]) }}">{{ $category->name }}</a><span class="has-subnav"><i class="fa fa-angle-down"></i></span></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li><a href="#">{{ __('menu.popular') }}</a><span class="has-subnav"><i class="fa fa-angle-down"></i></span></li>
                                <li><a href="#">{{ __('menu.about') }}</a><span class="has-subnav"><i class="fa fa-angle-down"></i></span></li>
                            </ul>
                        </nav>
                        <!--// Navigation \\-->
                        @auth
                            <a href="#" class="ereaders-simple-btn ereaders-bgcolor">{{ __('menu.featured') }}</a>
                        @else
                            <a href="{{ route('login') }}" class="ereaders-simple-btn ereaders-bgcolor">{{ __('menu.dashboard') }}</a>
                        @endauth

                    </aside>
                </div>
            </div>
        </div>
    </header>
    <!--// Header \\-->

   @yield('content')

    <!--// Footer \\-->
    <footer id="ereaders-footer" class="ereaders-footer-one">

        <!--// Footer Widget \\-->

        <!--// Footer Widget \\-->

        <!--// CopyRight \\-->
        <div class="ereaders-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p><i class="fa fa-copyright"></i> {{ date('Y') }}, {{ __('ebook.copyright') }}</p>
                        <ul class="footer-social-network">
                            <li><a href="facebook.com" class="fa fa-facebook"></a></li>
                            <li><a href="linkedin.com" class="fa fa-linkedin"></a></li>
                            <li><a href="twitter.com" class="fa fa-twitter"></a></li>
                            <li><a href="pinterest.com" class="fa fa-pinterest-p"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--// CopyRight \\-->

    </footer>
    <!--// Footer \\-->

    <div class="clearfix"></div>

</div>
<!--// Main Wrapper \\-->

<!-- jQuery (necessary for JavaScript plugins) -->
<script type="text/javascript" src="/script/jquery.js"></script>
<script type="text/javascript" src="/script/jquery-ui.js"></script>
<script type="text/javascript" src="/script/bootstrap.min.js"></script>
<script type="text/javascript" src="/script/slick.slider.min.js"></script>
<script type="text/javascript" src="/script/fancybox.pack.js"></script>
<script type="text/javascript" src="/script/isotope.min.js"></script>
<script type="text/javascript" src="/script/progressbar.js"></script>
<script type="text/javascript" src="/script/jquery.countdown.min.js"></script>
<script type="text/javascript" src="/script/circle-chart.js"></script>
<script type="text/javascript" src="/script/numscroller.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js@key=AIzaSyAs_m2ywm-bE8z9YM_WmtPaIQNIosN4Dxo&callback=initMap"></script>
<script type="text/javascript" src="/script/functions.js"></script>

</body>
</html>
