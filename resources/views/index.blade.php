@extends('layouts.ebook')

@section('content')
    <!--// Main Banner \\-->
    <div class="ereaders-banner">

        <!--// Slider \\-->

        @if(count($sliders) > 0)
            @foreach($sliders as $slider)
                <div class="ereaders-banner-layer">
                    <img src="/{{ $slider->bg_image }}" alt="">
                    <span class="ereaders-banner-transparent"></span>
                    <div class="ereaders-banner-caption">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="ereaders-banner-text-two">
                                        <h2>{{ $slider->header }}</h2>
                                        <h3>{{ $slider->second_text }}</h3>
                                        <p>{{ $slider->description }}</p>
                                        <a href="{{ $slider->link }}" class="ereaders-simple-btn ereaders-bgcolor">{{ __('ebook.button_detail') }}</a>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="ereaders-banner-thumb"><img src="/{{ $slider->image }}" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
    @endif
    <!--// Slider \\-->

    </div>
    <!--// Main Banner \\-->

    <!--// Main Content \\-->
    <div class="ereaders-main-content ereaders-content-padding">

        <!--// Main Section \\-->
        <div class="ereaders-main-section ereaders-blog-gridfull">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ereaders-fancy-title">
                            <h2>{{ __('ebook.latest_articles') }}</h2>

                        </div>
                        <div class="ereaders-blog ereaders-blog-grid">
                            <ul class="row">
                                <li class="col-md-4">
                                    <div class="ereaders-blog-grid-wrap">
                                        <figure><a href="blog-detail.html"><img src="extra-images/blog-grid-img1.jpg" alt=""></a></figure>
                                        <div class="ereaders-blog-grid-text">
                                            <span>Business</span>
                                            <h2><a href="blog-detail.html">Things you should know before pairing your home</a></h2>
                                            <ul class="ereaders-blog-option">
                                                <li>21 August 2017</li>
                                                <li><a href="404.html"> Comments 32</a></li>
                                            </ul>
                                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. </p>
                                            <a href="blog-detail.html" class="ereaders-readmore-btn">Read more <i class="fa fa-angle-double-right"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="ereaders-blog-grid-wrap">
                                        <figure><a href="blog-detail.html"><img src="extra-images/blog-grid-img2.jpg" alt=""></a></figure>
                                        <div class="ereaders-blog-grid-text">
                                            <span>Domain</span>
                                            <h2><a href="blog-detail.html">consectetur adipiscing elit. Pra esent accumsan</a></h2>
                                            <ul class="ereaders-blog-option">
                                                <li>21 August 2017</li>
                                                <li><a href="404.html"> Comments 32</a></li>
                                            </ul>
                                            <p>Lorem ipsum dolor sit amet, consectetu ad piscing elit. Praesent accumsan sit amet risus sed auctor. </p>
                                            <a href="blog-detail.html" class="ereaders-readmore-btn">Read more <i class="fa fa-angle-double-right"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-4">
                                    <div class="ereaders-blog-grid-wrap">
                                        <figure><a href="blog-detail.html"><img src="extra-images/blog-grid-img3.jpg" alt=""></a></figure>
                                        <div class="ereaders-blog-grid-text">
                                            <span>Keywords</span>
                                            <h2><a href="blog-detail.html">Ut non imperdiet quam Nullam sed lacus ut mi ornare</a></h2>
                                            <ul class="ereaders-blog-option">
                                                <li>21 August 2017</li>
                                                <li><a href="404.html"> Comments 32</a></li>
                                            </ul>
                                            <p>Nullam sed lacus ut mi ornare consectetur id sed urna. Praesent vestibulum eget ante nec semper.</p>
                                            <a href="blog-detail.html" class="ereaders-readmore-btn">Read more <i class="fa fa-angle-double-right"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Main Section \\-->

        <!--// Main Section \\-->
        <div class="ereaders-main-section ereaders-app-sectionfull">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <div class="ereaders-fancy-title">
                            <h2>Read It In All Devices</h2>
                            <div class="clearfix"></div>
                            <p>Large online bookstores offer used books for sale, too. Individuals wishing to sell their used Books</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="ereaders-app-text">
                            <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit aesent accumsan sit amet risus sed auctor. </h5>
                            <p>Aenean ultricies iaculis cursus. Mauris enim tellus, finibus in felis sollicitu din iaculis dolor. Donec sollicitudin orci id efficitur dapibus. Aliq uamtem por ien sed condimentum fringilla. In magna elit, ultrices a eros it amet, iaculis hendrerit tellusn iaculis nisi, nec maximus lorem.</p>
                            <p>Nam ut egestas nibh. Phasellus sollicitudin tempus neque quis gravida. Aenean a eros at ex pharetra suscipit. </p>
                            <a href="404.html" class="ereaders-fancy-btn"><i class="icon ereaders-apple-logo"></i> <span><small>Download on the</small><br> App Store</span></a>
                            <a href="404.html" class="ereaders-fancy-btn"><i class="icon ereaders-play-store"></i> <span><small>GET IT ON</small><br> GooglePlay</span></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ereaders-app-thumb"><img src="extra-images/app-thumb.jpg" alt=""></div>
                    </div>

                </div>
            </div>
        </div>
        <!--// Main Section \\-->

        <!--// Main Section \\-->
        <div class="ereaders-main-section ereaders-partner-sliderfull">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ereaders-fancy-title">
                            <h2>Our sponsored</h2>
                            <div class="clearfix"></div>
                            <p>Large online bookstores offer used books for sale, too. Individuals wishing to sell their used Books</p>
                        </div>
                        <div class="ereaders-partner-slider">
                            <div class="ereaders-partner-slider-layer"> <a data-fancybox-group="group" href="extra-images/partner-logo-1.png" class="fancybox"><img src="extra-images/partner-logo-1.png" alt=""></a> </div>
                            <div class="ereaders-partner-slider-layer"> <a data-fancybox-group="group" href="extra-images/partner-logo-2.png" class="fancybox"><img src="extra-images/partner-logo-2.png" alt=""></a> </div>
                            <div class="ereaders-partner-slider-layer"> <a data-fancybox-group="group" href="extra-images/partner-logo-3.png" class="fancybox"><img src="extra-images/partner-logo-3.png" alt=""></a> </div>
                            <div class="ereaders-partner-slider-layer"> <a data-fancybox-group="group" href="extra-images/partner-logo-4.png" class="fancybox"><img src="extra-images/partner-logo-4.png" alt=""></a> </div>
                            <div class="ereaders-partner-slider-layer"> <a data-fancybox-group="group" href="extra-images/partner-logo-5.png" class="fancybox"><img src="extra-images/partner-logo-5.png" alt=""></a> </div>
                            <div class="ereaders-partner-slider-layer"> <a data-fancybox-group="group" href="extra-images/partner-logo-2.png" class="fancybox"><img src="extra-images/partner-logo-2.png" alt=""></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Main Section \\-->

    </div>
    <!--// Main Content \\-->
@endsection
