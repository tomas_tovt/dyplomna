<?php
return [
    'button_detail' => 'Детальніше',
    'login_title' => 'Вхід на сайт',
    'login' => 'Вхід',
    'login_title_secondary' => 'Вхід для зареєстрованих користувачів',
    'e-mail' => 'E-Mail',
    'remember_me' => 'Запам\'ятати мене',
    'password' => 'Пароль',
    'password_confirm' => 'Підтвердіть пароль',
    'homepage' => 'Головна',
    'copyright' => 'Всі права захищені, Кус Віталій Федорович',
    'our_publishers' => 'Наші видавництва',
    'register_text' => 'Зареєструватися',
    'register_title' => 'Реєстрація',
    'register_title_secondary' => 'Реєстрація нового користувача',
    'name' => 'Ім\'я користувача',
    'latest_articles' => 'Останні статті',
];
