<?php
return [
    'home' => 'Головна',
    'books' => 'Книги',
    'popular' => 'Популярне',
    'dashboard' => 'Особистий кабінет',
    'about' => 'Про Нас',
    'featured' => 'Мої закладки',
];
